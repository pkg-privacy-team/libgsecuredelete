dnl GSD_PATH_PROG(VAR, PROG, [DEFAULT])
dnl --------------------------------
dnl Checks for a program like AC_PATH_PROG but supports overwriting with a
dnl --with option. With argument yes (or check, the default), search for the
dnl program with AC_PATH_PROG; with argument no (or default), use the default
dnl value, and with any other value, use this value as the path to the program.
AC_DEFUN([GSD_PATH_PROG],
[
  AC_ARG_WITH([$2],
              [AS_HELP_STRING([--with-m4_tolower($2)=PATH],
                              [Specify the path to the $2 program])],
              [],
              [m4_tolower(AS_TR_SH([with_$2]))=check])
  
  AS_CASE([$m4_tolower(AS_TR_SH([with_$2]))],
          [check|yes],  [AC_PATH_PROG([$1], [$2], [$3])],
          [default|no], [AC_MSG_CHECKING([for $2])
                         AS_TR_SH([$1])="$3"
                         AC_MSG_RESULT([$AS_TR_SH([$1])])],
          [AC_MSG_CHECKING([for $2])
           AS_TR_SH([$1])="$m4_tolower(AS_TR_SH([with_$2]))"
           AC_MSG_RESULT([$AS_TR_SH([$1])])])
])
