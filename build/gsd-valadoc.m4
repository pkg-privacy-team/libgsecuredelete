# Checks for valadoc in `PATH' and define the VALADOC variable if found.
# Optionally a minimum version number can be requested.
#
# GSD_VALADOC_CHECK([MINIMUM-VERSION])
# ------------------------------------
AC_DEFUN([GSD_VALADOC_CHECK],
[
  AC_ARG_ENABLE([valadoc],
                [AS_HELP_STRING([--enable-valadoc],
                                [Whether to build the documentation @<:@default=auto@:>@])],
                [enable_valadoc="$enableval"],
                [enable_valadoc=auto])
  
  have_valadoc=no
  AS_IF([test "x$enable_valadoc" != xno],
        [AC_PATH_PROG([VALADOC], [valadoc], [NONE])
         AS_IF([test "x$VALADOC" != xNONE],
               [AC_MSG_CHECKING([whether $VALADOC is at least version $1])
                gsd__valadoc_version=`$VALADOC --version | sed 's/Valadoc  *//'`
                AS_VERSION_COMPARE([$1], ["$gsd__valadoc_version"],
                                   [have_valadoc=yes],
                                   [have_valadoc=yes],
                                   [have_valadoc=no])
                AS_IF([test "x$have_valadoc" != xyes],
                      [AC_MSG_RESULT([no])],
                      [AC_MSG_RESULT([yes])
                       AC_SUBST([VALADOC])])])
         AS_IF([test "x$have_valadoc" != xyes],
               [AS_IF([test "x$enable_valadoc" = xyes],
                      [AC_MSG_ERROR([valadoc not found or too old])])])])
  AM_CONDITIONAL([ENABLE_VALADOC], [test "x$have_valadoc" = xyes])
])
