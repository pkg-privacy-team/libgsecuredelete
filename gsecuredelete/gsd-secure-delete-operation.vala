/* 
 * 
 * Copyright (C) 2009-2011 Colomban Wendling <ban@herbesfolles.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

using GLib;

namespace Gsd
{
  /**
   * An AsyncOperation subclass to implement SecureDelete operations.
   * 
   * This class implements everything that is shared by all SecureDelete
   * operators.
   */
  public abstract class SecureDeleteOperation : AsyncOperation
  {
    /**
     * Security mode
     */
    public enum Mode {
      /**
       * normal mode (default) (38 passes)
       */
      NORMAL,
      /**
       * less security (-l option) (2 passes)
       */
      INSECURE,
      /**
       * even less security (-ll option) (1 pass)
       */
      VERY_INSECURE
    }
    
    /**
     * Whether to use fast (and insecure) work mode (-f option).
     */
    public bool fast {
      get;
      set;
      default = false;
    }
    
    /**
     * The security mode
     */
    public Mode mode {
      get;
      set;
      default = Mode.NORMAL;
    }
    
    /* returns the argument to pass to SecureDelete tools depending on the
     * speed mode. %null can be returned, meaning no argument should be
     * added */
    private unowned string? get_argument_for_fast (bool fast)
    {
      if (fast) {
        return "-f";
      }
      return null;
    }
    
    /* adds the argument needed by the current set speed to an argument list */
    private void add_fast_argument (ref List<string> args)
    {
      unowned string? arg_fast = this.get_argument_for_fast (this.fast);
      if (arg_fast != null) {
        args.append (arg_fast);
      }
    }
    
    /* returns the argument to pass to SecureDelete tools depending on the
     * security mode. %null can be returned, meaning no argument should be
     * added */
    private unowned string? get_argument_for_mode (Mode mode)
    {
      switch (mode) {
        case Mode.INSECURE:
          return "-l";
        case Mode.VERY_INSECURE:
          return "-ll";
      }
      return null;
    }
    
    /* adds the argument needed by the current set mode to an argument list */
    private void add_mode_argument (ref List<string> args)
    {
      unowned string? arg_mode = this.get_argument_for_mode (this.mode);
      if (arg_mode != null) {
        args.append (arg_mode);
      }
    }
    
    protected override List<string> build_args ()
      throws AsyncOperationError
    {
      List<string> args = null;
      
      args.append ("-v");
      this.add_fast_argument (ref args);
      this.add_mode_argument (ref args);
      
      return args;
    }
    
    /* returns the supposed number of pass to show the progress status */
    protected override uint get_max_progress ()
    {
      /* FIXME: Get the pass number reported by the program? */
      switch (this.mode) {
        case Mode.INSECURE:
          return 2;
        case Mode.VERY_INSECURE:
          return 1;
        default:
          return 38;
      }
    }
    
    /* gets the progress status of the child process */
    protected override uint get_progress ()
    {
      uint progress = 0;
      
      try {
        progress = FD.count_ready_bytes (this.fd_out, '*');
      } catch (FDError e) {
        warning ("Progression check failed: %s", e.message);
      }
      
      return progress;
    }
    
    /**
     * Runs a SecureDelete operator asynchronously.
     * 
     * @return whether operation started successfully.
     */
    public new bool run ()
      throws SpawnError, AsyncOperationError
    {
      return base.run (null, 0);
    }
    
    /**
     * Runs a SecureDelete operator synchronously.
     * 
     * @return whether operation was successful.
     */
    public new bool run_sync ()
      throws SpawnError, AsyncOperationError
    {
      return base.run_sync (null, SpawnFlags.STDOUT_TO_DEV_NULL, null);
    }
  }
}
