/* 
 * 
 * Copyright (C) 2009-2011 Colomban Wendling <ban@herbesfolles.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

using GLib;

/* interval (in milliseconds) between two checks for subprocess' status.
 * A too short value may use too much CPU and a too long may be less
 * responsive. */
private const uint WATCH_INTERVAL = 100;

namespace Gsd
{
  /**
   * Error domain for the AsyncOperation class.
   */
  public errordomain AsyncOperationError {
    /**
     * The subprocess crashed or terminated with an error.
     */
    CHILD_FAILED,
    /**
     * Other error from a subclass of AsyncOperation.
     * 
     * This is used as a workaround for the lack of error subclassing,
     * disallowing to manage "generic" errors cleanly.
     */
    SUBCLASS_ERROR /* workaround the miss of error subclassing */
  }
  
  /* 
   * TODO:
   *  * Be able to launch the operation as root/another user.
   */
  /**
   * An asynchronous process spawner, with support for progression and success
   * report.
   * 
   * This is a base class designed to be subclassed by actual spawners, with
   * as less efforts as possible.
   * 
   * To subclass this class, the only thing you need to implement is the
   * argument builder, that gives the arguments of the spawned command; and you
   * need to set the AsyncOperation::path property to the command to spawn in
   * your constructor too (hence it is not required, it is slightly better to
   * set it since it must be set before calling run() or run_sync() - or getting
   * it).
   * This said, you may want to override get_max_progress() and get_progress()
   * to add actual progress support; and build_env() to provide a specific
   * environment to your command.
   * 
   * =Important note=
   * 
   * As this class uses a timeout function to watch the child, you need a GLib
   * MainLoop for the watching process to work for the asynchronous method.
   * 
   * =Simple usage of an hypothetical FooOperation subclass=
   * 
   * {{{
   * var foo = new FooOperation();
   * 
   * // Connect the finish callback
   * foo.finished.connect ((success, error) => {
   *                         if (success) {
   *                           stdout.printf ("success!\n");
   *                         } else {
   *                           stderr.printf ("failure: %s\n", error);
   *                         }
   *                       });
   * // and the progress callback
   * foo.progress.connect ((progress) => {
   *                         stdout.printf ("\r%.0f%%", progress * 100);
   *                       });
   * foo.run ();
   * 
   * var loop = GLib.MainLoop (null, false);
   * loop.run ();
   * }}}
   */
  public abstract class AsyncOperation : Object
  {
    /* possible subprocess exit status classification */
    protected enum ExitStatus {
      /* full success */
      SUCCESS,
      /* success, but warnings issued (possibly non-fatal error) */
      WARNING,
      /* error (fatal failures) */
      ERROR
    }
    
    /* fields for async operation */
    /**
     * The maximum number of progress steps.
     */
    protected uint  n_passes;
    /**
     * the current progress step
     */
    protected uint  passes;
    /**
     * the subprocess PID
     */
    protected Pid   pid;
    /**
     * the subprocess' standard input
     */
    protected int   fd_in;
    /**
     * the subprocess' standard output
     */
    protected int   fd_out;
    /**
     * the subprocess' error output
     */
    protected int   fd_err;
    private bool    _busy = false;
    private string  _path = null;
    private string  _path_default = null;
    
    /* signals */
    /**
     * This signal is emitted when the operation just terminated.
     * 
     * @param success whether the operation succeed.
     * @param message any error or warning message.
     */
    public signal void finished (bool     success,
                                 string?  message);
    /**
     * This signal is emitted when the progress status of the operation changes.
     * 
     * @param fraction the current progress, from 0.0 to 1.0.
     */
    public signal void progress (double   fraction);
    
    /* properties */
    /**
     * The path to the command to spawn.
     * 
     * Setting it to null resets it to its default value.
     */
    public string path {
      get {
        if (this._path == null) {
          critical ("Property AsyncOperation::path not set");
        }
        return this._path;
      }
      /* We fake the default value overriding by getting the first set as it.
       * Then, subclasses should set this property to their default value at
       * construction time. */
      set {
        if (value != null) {
          if (this._path_default == null) {
            this._path_default = value;
          }
          this._path = value;
        } else {
          this._path = this._path_default;
        }
      }
    }
    /**
     * Whether the operation object is busy.
     * 
     * A busy operation cannot be reused until it gets ready again.
     * An operation is busy when it is currently doing a job.
     */
    public bool busy {
      public get {
        return this._busy;
      }
    }
    
    /* builds the command's arguments (argv) */
    protected abstract List<string> build_args ()
      throws AsyncOperationError;
    
    /* converts the args built by build_args() to the expected format, and add
     * the command itself */
    private string?[] do_build_args ()
      throws AsyncOperationError
    {
      List<string>  args;
      string?[]     array_args;
      size_t        i;
      
      args = this.build_args ();
      array_args = new string[args.length () + 2];
      i = 0;
      array_args[i++] = this.path;
      foreach (unowned string arg in args) {
        array_args[i++] = arg;
      }
      array_args[i] = null;
      
      return array_args;
    }
    
    /* builds the command's environment */
    protected virtual string?[]? build_env ()
    {
      return null;
    }
    
    /* called after subprocess termination to cleanup anything that may be
     * cleaned up (I think about some list of files that was just treated or so)
     */
    protected virtual void cleanup ()
    {
      
    }
    
    /* returns the value at which the progress is full. When the progress reach
     * this value, it will be considered to be at 100% (This has no impact
     * other than reporting the current progress state).
     */
    protected virtual uint get_max_progress ()
    {
      return 0;
    }
    
    /* gets the additional progress status of the child process.
     * This function gets called from time to time to get the new additional
     * progress of the operation, since last call.
     * Then, the combination of the returned values of repeated calls to this
     * function should be equal to the value returned by get_max_progress() in
     * order to report a meaningful progress status.
     */
    protected virtual uint get_progress ()
    {
      return 0;
    }
    
    /* updates the progress status and emit AsyncOperation::progress if
     * changed */
    private void update_progress ()
    {
      uint progress = this.get_progress ();
      if (progress > 0) {
        this.passes += progress;
        this.progress (this.passes / (this.n_passes * 1.0));
      }
    }
    
    /* gets the standard error of the subprocess
     * this function never fails. if the error output cannot be read, return
     * "???" and send a warning.
     * don't call this function before the subprocess termination as it blocks
     * until subprocess error output's EOF */
    protected virtual string? get_subprocess_error_msg ()
    {
      /* don't worry about blocking as we call this after the subprocess
       * termination, then the fd have proper EOF */
      try {
        return FD.read_string (this.fd_err);
      } catch (FDError e) {
        warning ("Subprocess error output read failed: %s", e.message);
        return "???";
      }
    }
    
    /* Classifies the subprocess exit status */
    protected virtual ExitStatus classify_exit_status (int exit_status)
    {
      return exit_status == 0 ? ExitStatus.SUCCESS : ExitStatus.ERROR;
    }
    
    /* checks if the child finished, and dispatches related things.
     * 
     * It is meant to be called as a timeout function.
     */
    private bool do_wait_child ()
    {
      bool        finished = true;
      bool        success  = false;
      int         exit_status;
      Posix.pid_t wait_rv;
      string?     message  = null;
      
      wait_rv = Posix.waitpid ((Posix.pid_t)this.pid, out exit_status,
                               Posix.WNOHANG);
      if ((int)wait_rv < 0) {
        critical ("waitpid() failed: %s", strerror (errno));
        /* if we cannot watch the child, try to kill it */
        if (Posix.kill ((Posix.pid_t)this.pid, ProcessSignal.TERM) < 0) {
          critical ("kill() failed: %s", strerror (errno));
        }
      } else if ((int)wait_rv == 0) {
        /* nothing to do, just wait until next call */
        this.update_progress ();
        finished = false;
      } else {
        if (! Process.if_exited (exit_status)) {
          message = _("Subprocess crashed.") + "\n" + get_subprocess_error_msg ();
        } else {
          int         status_int  = Process.exit_status (exit_status);
          ExitStatus  status      = this.classify_exit_status (status_int);
          
          if (status == ExitStatus.WARNING) {
            message = get_subprocess_error_msg ();
          } else if (status == ExitStatus.ERROR) {
            message = _("Subprocess failed.") + "\n" + get_subprocess_error_msg ();
          }
          
          success = (status != ExitStatus.ERROR);
        }
      }
      
      if (finished) {
        this.update_progress ();
        this.finished (success, message);
        
        this.cleanup ();
        Process.close_pid (this.pid);
        Posix.close (this.fd_err);
        Posix.close (this.fd_out);
        Posix.close (this.fd_in);
        lock (this._busy) {
          this._busy = false;
        }
      }
      
      /* return false until subprocess terminated to keep being called by the
       * timeout manager. */
      return ! finished;
    }
    
    private bool signal_child (ProcessSignal signum)
      requires (this.busy)
    {
      int kill_status;
      
      kill_status = Posix.killpg ((Posix.pid_t)this.pid, signum);
      if (kill_status < 0) {
        critical ("kill() failed: %s", strerror (errno));
      }
      
      return ! (kill_status < 0);
    }
    
    /**
     * Tries to cancel the operation.
     * 
     * @return true if successfully canceled, false otherwise.
     */
    public bool cancel ()
    {
      /* we need to also send SIGCONT in order for the child to have a chance
       * to actually quit if it was stopped. */
      return (signal_child (ProcessSignal.TERM) &&
              signal_child (ProcessSignal.CONT));
    }
    
    /**
     * Tries to pause the operation.
     * 
     * @return true if successfully paused, false otherwise.
     */
    public bool pause ()
    {
      return signal_child (ProcessSignal.STOP);
    }
    
    /**
     * Tries to resume the operation.
     * 
     * @return true if successfully resumed, false otherwise.
     */
    public bool resume ()
    {
      return signal_child (ProcessSignal.CONT);
    }
    
    private void child_setup ()
    {
      /* Set the process group ID (PGID) of the child to its PID.
       * This allows us to use killpg(), which is needed if we want to be able
       * to stop grandchildren (assuming they didn't live the group).
       * 
       * We need this for the gsd-sfill-helper case so we can send SIGSTOP not
       * only to the controlling script but also all its asynchronous children.
       * It also simplifies the script implementation for cancellation as it
       * allows us to cancel all children at once ourselves. */
      Posix.setpgid (0, 0);
    }
    
    /**
     * Launches an operation asynchronously.
     * 
     * @param working_directory the working directory of the child process, or
     *    null to use the parent's one.
     * @param spawn_flags SpawnFlags. You may only use the SEARCH_PATH and
     *    CHILD_INHERITS_STDIN flags, others may conflict.
     * 
     * @return whether asynchronous operation was successfully started.
     */
    public bool run (string?    working_directory = null,
                     SpawnFlags spawn_flags = 0)
      throws SpawnError, AsyncOperationError
    {
      bool success = false;
      bool busy    = false;
      
      lock (this._busy) {
        busy = this._busy;
        /* no need to test the value as it is either already true or we need to
         * set it to true */
        this._busy = true;
      }
      return_val_if_fail (! busy, false);
      try {
        this.fd_err = -1;
        this.fd_out = -1;
        this.n_passes = this.get_max_progress ();
        this.passes = 0;
        success = Process.spawn_async_with_pipes (
          working_directory, this.do_build_args (), this.build_env (),
          spawn_flags | SpawnFlags.DO_NOT_REAP_CHILD, this.child_setup,
          out this.pid, out this.fd_in, out this.fd_out, out this.fd_err
        );
        if (success) {
          Timeout.add (WATCH_INTERVAL, this.do_wait_child);
        }
      } finally {
        /* if success is false, an error was thrown and the timeout not added,
         * then release the busy lock here */
        if (! success) {
          lock (this._busy) {
            this._busy = false;
          }
        }
      }
      
      return success;
    }
    
    /**
     * Launches an operation synchronously.
     * 
     * @param working_directory the working directory of the child process, or
     *    null to use the parent's one.
     * @param spawn_flags SpawnFlags. You may only use the SEARCH_PATH and
     *    CHILD_INHERITS_STDIN flags, others may conflict.
     * @param standard_output return location for the subprocess' standard
     *    output, or null to ignore it.
     * 
     * @return true if all worked properly, and false if something failed.
     *         An error is thrown if something fails. It can be the subprocess
     *         spawning (SpawnError) or the child that failed
     *         (AsyncOperationError.CHILD_FAILED).
     */
    public bool run_sync (string?    working_directory = null,
                          SpawnFlags spawn_flags = 0,
                          out string standard_output = null)
      throws SpawnError, AsyncOperationError
    {
      bool    success = true;
      bool    busy    = false;
      string  error_output;
      int     exit_status;
      
      lock (this._busy) {
        busy = this._busy;
        this._busy = true;
      }
      return_val_if_fail (! busy, false);
      try {
        success = Process.spawn_sync (working_directory, this.do_build_args (),
                                      this.build_env (), spawn_flags, null,
                                      out standard_output, out error_output,
                                      out exit_status);
        if (success) {
          string? message = null;
          
          success = false;
          if (! Process.if_exited (exit_status)) {
            message = _("Subprocess crashed.") + "\n" + error_output;
          } else if (Process.exit_status (exit_status) != 0) {
            message = _("Subprocess failed.") + "\n" + error_output;
          } else {
            success = true;
          }
          this.finished (success, message);
          this.cleanup ();
          if (message != null) {
            throw new AsyncOperationError.CHILD_FAILED ("%s", message);
          }
        }
      } finally {
        lock (this._busy) {
          this._busy = false;
        }
      }
      
      return success;
    }
  }
}
