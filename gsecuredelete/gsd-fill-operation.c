/* gsd-fill-operation.c generated by valac 0.34.2, the Vala compiler
 * generated from gsd-fill-operation.vala, do not modify */

/* 
 * 
 * Copyright (C) 2009-2011 Colomban Wendling <ban@herbesfolles.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include <glib.h>
#include <glib-object.h>
#include <stdlib.h>
#include <string.h>
#include <gsd-config.h>


#define GSD_TYPE_ASYNC_OPERATION (gsd_async_operation_get_type ())
#define GSD_ASYNC_OPERATION(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), GSD_TYPE_ASYNC_OPERATION, GsdAsyncOperation))
#define GSD_ASYNC_OPERATION_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), GSD_TYPE_ASYNC_OPERATION, GsdAsyncOperationClass))
#define GSD_IS_ASYNC_OPERATION(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GSD_TYPE_ASYNC_OPERATION))
#define GSD_IS_ASYNC_OPERATION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GSD_TYPE_ASYNC_OPERATION))
#define GSD_ASYNC_OPERATION_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), GSD_TYPE_ASYNC_OPERATION, GsdAsyncOperationClass))

typedef struct _GsdAsyncOperation GsdAsyncOperation;
typedef struct _GsdAsyncOperationClass GsdAsyncOperationClass;
typedef struct _GsdAsyncOperationPrivate GsdAsyncOperationPrivate;

#define GSD_ASYNC_OPERATION_TYPE_EXIT_STATUS (gsd_async_operation_exit_status_get_type ())

#define GSD_TYPE_SECURE_DELETE_OPERATION (gsd_secure_delete_operation_get_type ())
#define GSD_SECURE_DELETE_OPERATION(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), GSD_TYPE_SECURE_DELETE_OPERATION, GsdSecureDeleteOperation))
#define GSD_SECURE_DELETE_OPERATION_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), GSD_TYPE_SECURE_DELETE_OPERATION, GsdSecureDeleteOperationClass))
#define GSD_IS_SECURE_DELETE_OPERATION(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GSD_TYPE_SECURE_DELETE_OPERATION))
#define GSD_IS_SECURE_DELETE_OPERATION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GSD_TYPE_SECURE_DELETE_OPERATION))
#define GSD_SECURE_DELETE_OPERATION_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), GSD_TYPE_SECURE_DELETE_OPERATION, GsdSecureDeleteOperationClass))

typedef struct _GsdSecureDeleteOperation GsdSecureDeleteOperation;
typedef struct _GsdSecureDeleteOperationClass GsdSecureDeleteOperationClass;
typedef struct _GsdSecureDeleteOperationPrivate GsdSecureDeleteOperationPrivate;

#define GSD_TYPE_ZEROABLE_OPERATION (gsd_zeroable_operation_get_type ())
#define GSD_ZEROABLE_OPERATION(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), GSD_TYPE_ZEROABLE_OPERATION, GsdZeroableOperation))
#define GSD_ZEROABLE_OPERATION_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), GSD_TYPE_ZEROABLE_OPERATION, GsdZeroableOperationClass))
#define GSD_IS_ZEROABLE_OPERATION(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GSD_TYPE_ZEROABLE_OPERATION))
#define GSD_IS_ZEROABLE_OPERATION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GSD_TYPE_ZEROABLE_OPERATION))
#define GSD_ZEROABLE_OPERATION_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), GSD_TYPE_ZEROABLE_OPERATION, GsdZeroableOperationClass))

typedef struct _GsdZeroableOperation GsdZeroableOperation;
typedef struct _GsdZeroableOperationClass GsdZeroableOperationClass;
typedef struct _GsdZeroableOperationPrivate GsdZeroableOperationPrivate;

#define GSD_TYPE_FILL_OPERATION (gsd_fill_operation_get_type ())
#define GSD_FILL_OPERATION(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), GSD_TYPE_FILL_OPERATION, GsdFillOperation))
#define GSD_FILL_OPERATION_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), GSD_TYPE_FILL_OPERATION, GsdFillOperationClass))
#define GSD_IS_FILL_OPERATION(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GSD_TYPE_FILL_OPERATION))
#define GSD_IS_FILL_OPERATION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GSD_TYPE_FILL_OPERATION))
#define GSD_FILL_OPERATION_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), GSD_TYPE_FILL_OPERATION, GsdFillOperationClass))

typedef struct _GsdFillOperation GsdFillOperation;
typedef struct _GsdFillOperationClass GsdFillOperationClass;
typedef struct _GsdFillOperationPrivate GsdFillOperationPrivate;

#define GSD_FILL_OPERATION_TYPE_WIPE_MODE (gsd_fill_operation_wipe_mode_get_type ())
#define _g_free0(var) (var = (g_free (var), NULL))
#define __g_list_free__g_free0_0(var) ((var == NULL) ? NULL : (var = (_g_list_free__g_free0_ (var), NULL)))
#define _vala_assert(expr, msg) if G_LIKELY (expr) ; else g_assertion_message_expr (G_LOG_DOMAIN, __FILE__, __LINE__, G_STRFUNC, msg);
#define _vala_return_if_fail(expr, msg) if G_LIKELY (expr) ; else { g_return_if_fail_warning (G_LOG_DOMAIN, G_STRFUNC, msg); return; }
#define _vala_return_val_if_fail(expr, msg, val) if G_LIKELY (expr) ; else { g_return_if_fail_warning (G_LOG_DOMAIN, G_STRFUNC, msg); return val; }
#define _vala_warn_if_fail(expr, msg) if G_LIKELY (expr) ; else g_warn_message (G_LOG_DOMAIN, __FILE__, __LINE__, G_STRFUNC, msg);

typedef enum  {
	GSD_ASYNC_OPERATION_ERROR_CHILD_FAILED,
	GSD_ASYNC_OPERATION_ERROR_SUBCLASS_ERROR
} GsdAsyncOperationError;
#define GSD_ASYNC_OPERATION_ERROR gsd_async_operation_error_quark ()
typedef enum  {
	GSD_ASYNC_OPERATION_EXIT_STATUS_SUCCESS,
	GSD_ASYNC_OPERATION_EXIT_STATUS_WARNING,
	GSD_ASYNC_OPERATION_EXIT_STATUS_ERROR
} GsdAsyncOperationExitStatus;

struct _GsdAsyncOperation {
	GObject parent_instance;
	GsdAsyncOperationPrivate * priv;
	guint n_passes;
	guint passes;
	GPid pid;
	gint fd_in;
	gint fd_out;
	gint fd_err;
};

struct _GsdAsyncOperationClass {
	GObjectClass parent_class;
	GList* (*build_args) (GsdAsyncOperation* self, GError** error);
	gchar** (*build_env) (GsdAsyncOperation* self, int* result_length1);
	void (*cleanup) (GsdAsyncOperation* self);
	guint (*get_max_progress) (GsdAsyncOperation* self);
	guint (*get_progress) (GsdAsyncOperation* self);
	gchar* (*get_subprocess_error_msg) (GsdAsyncOperation* self);
	GsdAsyncOperationExitStatus (*classify_exit_status) (GsdAsyncOperation* self, gint exit_status);
};

struct _GsdSecureDeleteOperation {
	GsdAsyncOperation parent_instance;
	GsdSecureDeleteOperationPrivate * priv;
};

struct _GsdSecureDeleteOperationClass {
	GsdAsyncOperationClass parent_class;
};

struct _GsdZeroableOperation {
	GsdSecureDeleteOperation parent_instance;
	GsdZeroableOperationPrivate * priv;
};

struct _GsdZeroableOperationClass {
	GsdSecureDeleteOperationClass parent_class;
};

struct _GsdFillOperation {
	GsdZeroableOperation parent_instance;
	GsdFillOperationPrivate * priv;
};

struct _GsdFillOperationClass {
	GsdZeroableOperationClass parent_class;
};

typedef enum  {
	GSD_FILL_OPERATION_WIPE_MODE_FULL,
	GSD_FILL_OPERATION_WIPE_MODE_INODES,
	GSD_FILL_OPERATION_WIPE_MODE_DATA
} GsdFillOperationWipeMode;

struct _GsdFillOperationPrivate {
	GsdFillOperationWipeMode _wipe_mode;
	gchar* _directory;
};


static gpointer gsd_fill_operation_parent_class = NULL;

GType gsd_async_operation_get_type (void) G_GNUC_CONST;
GQuark gsd_async_operation_error_quark (void);
GType gsd_async_operation_exit_status_get_type (void) G_GNUC_CONST;
GType gsd_secure_delete_operation_get_type (void) G_GNUC_CONST;
GType gsd_zeroable_operation_get_type (void) G_GNUC_CONST;
GType gsd_fill_operation_get_type (void) G_GNUC_CONST;
GType gsd_fill_operation_wipe_mode_get_type (void) G_GNUC_CONST;
#define GSD_FILL_OPERATION_GET_PRIVATE(o) (G_TYPE_INSTANCE_GET_PRIVATE ((o), GSD_TYPE_FILL_OPERATION, GsdFillOperationPrivate))
enum  {
	GSD_FILL_OPERATION_DUMMY_PROPERTY,
	GSD_FILL_OPERATION_WIPE_MODE,
	GSD_FILL_OPERATION_DIRECTORY
};
static const gchar* gsd_fill_operation_get_argument_for_wipe_mode (GsdFillOperation* self, GsdFillOperationWipeMode wipe_mode);
static void gsd_fill_operation_add_wipe_mode_argument (GsdFillOperation* self, GList** args);
GsdFillOperationWipeMode gsd_fill_operation_get_wipe_mode (GsdFillOperation* self);
static GList* gsd_fill_operation_real_build_args (GsdAsyncOperation* base, GError** error);
const gchar* gsd_fill_operation_get_directory (GsdFillOperation* self);
GList* gsd_async_operation_build_args (GsdAsyncOperation* self, GError** error);
static void _g_free0_ (gpointer var);
static void _g_list_free__g_free0_ (GList* self);
static void gsd_fill_operation_real_cleanup (GsdAsyncOperation* base);
void gsd_fill_operation_set_directory (GsdFillOperation* self, const gchar* value);
static GsdAsyncOperationExitStatus gsd_fill_operation_real_classify_exit_status (GsdAsyncOperation* base, gint exit_status);
gboolean gsd_fill_operation_run (GsdFillOperation* self, const gchar* directory, GError** error);
gboolean gsd_secure_delete_operation_run (GsdSecureDeleteOperation* self, GError** error);
gboolean gsd_fill_operation_run_sync (GsdFillOperation* self, const gchar* directory, GError** error);
gboolean gsd_secure_delete_operation_run_sync (GsdSecureDeleteOperation* self, GError** error);
GsdFillOperation* gsd_fill_operation_new (void);
GsdFillOperation* gsd_fill_operation_construct (GType object_type);
GsdZeroableOperation* gsd_zeroable_operation_construct (GType object_type);
void gsd_fill_operation_set_wipe_mode (GsdFillOperation* self, GsdFillOperationWipeMode value);
static GObject * gsd_fill_operation_constructor (GType type, guint n_construct_properties, GObjectConstructParam * construct_properties);
void gsd_async_operation_set_path (GsdAsyncOperation* self, const gchar* value);
static void gsd_fill_operation_finalize (GObject* obj);
static void _vala_gsd_fill_operation_get_property (GObject * object, guint property_id, GValue * value, GParamSpec * pspec);
static void _vala_gsd_fill_operation_set_property (GObject * object, guint property_id, const GValue * value, GParamSpec * pspec);


/**
     * What to wipe
     */
GType gsd_fill_operation_wipe_mode_get_type (void) {
	static volatile gsize gsd_fill_operation_wipe_mode_type_id__volatile = 0;
	if (g_once_init_enter (&gsd_fill_operation_wipe_mode_type_id__volatile)) {
		static const GEnumValue values[] = {{GSD_FILL_OPERATION_WIPE_MODE_FULL, "GSD_FILL_OPERATION_WIPE_MODE_FULL", "full"}, {GSD_FILL_OPERATION_WIPE_MODE_INODES, "GSD_FILL_OPERATION_WIPE_MODE_INODES", "inodes"}, {GSD_FILL_OPERATION_WIPE_MODE_DATA, "GSD_FILL_OPERATION_WIPE_MODE_DATA", "data"}, {0, NULL, NULL}};
		GType gsd_fill_operation_wipe_mode_type_id;
		gsd_fill_operation_wipe_mode_type_id = g_enum_register_static ("GsdFillOperationWipeMode", values);
		g_once_init_leave (&gsd_fill_operation_wipe_mode_type_id__volatile, gsd_fill_operation_wipe_mode_type_id);
	}
	return gsd_fill_operation_wipe_mode_type_id__volatile;
}


static const gchar* gsd_fill_operation_get_argument_for_wipe_mode (GsdFillOperation* self, GsdFillOperationWipeMode wipe_mode) {
	const gchar* result = NULL;
	GsdFillOperationWipeMode _tmp0_ = 0;
	g_return_val_if_fail (self != NULL, NULL);
	_tmp0_ = wipe_mode;
	switch (_tmp0_) {
		case GSD_FILL_OPERATION_WIPE_MODE_INODES:
		{
			result = "-i";
			return result;
		}
		case GSD_FILL_OPERATION_WIPE_MODE_DATA:
		{
			result = "-I";
			return result;
		}
		default:
		break;
	}
	result = NULL;
	return result;
}


static void gsd_fill_operation_add_wipe_mode_argument (GsdFillOperation* self, GList** args) {
	const gchar* arg_mode = NULL;
	GsdFillOperationWipeMode _tmp0_ = 0;
	const gchar* _tmp1_ = NULL;
	const gchar* _tmp2_ = NULL;
	g_return_if_fail (self != NULL);
	_tmp0_ = self->priv->_wipe_mode;
	_tmp1_ = gsd_fill_operation_get_argument_for_wipe_mode (self, _tmp0_);
	arg_mode = _tmp1_;
	_tmp2_ = arg_mode;
	if (_tmp2_ != NULL) {
		const gchar* _tmp3_ = NULL;
		gchar* _tmp4_ = NULL;
		_tmp3_ = arg_mode;
		_tmp4_ = g_strdup (_tmp3_);
		*args = g_list_append (*args, _tmp4_);
	}
}


static void _g_free0_ (gpointer var) {
	var = (g_free (var), NULL);
}


static void _g_list_free__g_free0_ (GList* self) {
	g_list_foreach (self, (GFunc) _g_free0_, NULL);
	g_list_free (self);
}


static GList* gsd_fill_operation_real_build_args (GsdAsyncOperation* base, GError** error) {
	GsdFillOperation * self;
	GList* result = NULL;
	const gchar* _tmp0_ = NULL;
	GList* args = NULL;
	GList* _tmp1_ = NULL;
	GList* _tmp2_ = NULL;
	GList* _tmp3_ = NULL;
	const gchar* _tmp4_ = NULL;
	gchar* _tmp5_ = NULL;
	GError * _inner_error_ = NULL;
	self = (GsdFillOperation*) base;
	_tmp0_ = self->priv->_directory;
	_vala_return_val_if_fail (_tmp0_ != NULL, "this.directory != null", NULL);
	args = NULL;
	_tmp2_ = GSD_ASYNC_OPERATION_CLASS (gsd_fill_operation_parent_class)->build_args ((GsdAsyncOperation*) G_TYPE_CHECK_INSTANCE_CAST (self, GSD_TYPE_ZEROABLE_OPERATION, GsdZeroableOperation), &_inner_error_);
	_tmp1_ = _tmp2_;
	if (G_UNLIKELY (_inner_error_ != NULL)) {
		if (_inner_error_->domain == GSD_ASYNC_OPERATION_ERROR) {
			g_propagate_error (error, _inner_error_);
			__g_list_free__g_free0_0 (args);
			return NULL;
		} else {
			__g_list_free__g_free0_0 (args);
			g_critical ("file %s: line %d: uncaught error: %s (%s, %d)", __FILE__, __LINE__, _inner_error_->message, g_quark_to_string (_inner_error_->domain), _inner_error_->code);
			g_clear_error (&_inner_error_);
			return NULL;
		}
	}
	_tmp3_ = _tmp1_;
	_tmp1_ = NULL;
	__g_list_free__g_free0_0 (args);
	args = _tmp3_;
	gsd_fill_operation_add_wipe_mode_argument (self, &args);
	_tmp4_ = self->priv->_directory;
	_tmp5_ = g_strdup (_tmp4_);
	args = g_list_append (args, _tmp5_);
	result = args;
	__g_list_free__g_free0_0 (_tmp1_);
	return result;
}


static void gsd_fill_operation_real_cleanup (GsdAsyncOperation* base) {
	GsdFillOperation * self;
	self = (GsdFillOperation*) base;
	gsd_fill_operation_set_directory (self, NULL);
}


static GsdAsyncOperationExitStatus gsd_fill_operation_real_classify_exit_status (GsdAsyncOperation* base, gint exit_status) {
	GsdFillOperation * self;
	GsdAsyncOperationExitStatus result = 0;
	gint _tmp0_ = 0;
	self = (GsdFillOperation*) base;
	_tmp0_ = exit_status;
	switch (_tmp0_) {
		case 0:
		{
			result = GSD_ASYNC_OPERATION_EXIT_STATUS_SUCCESS;
			return result;
		}
		case 42:
		{
			result = GSD_ASYNC_OPERATION_EXIT_STATUS_WARNING;
			return result;
		}
		default:
		{
			result = GSD_ASYNC_OPERATION_EXIT_STATUS_ERROR;
			return result;
		}
	}
}


/**
     * Launches a secure filling asynchronously.
     * 
     * @param directory the directory to fill. It is exactly the same as setting
     *    the FillOperation:directory property, just a convenience possibility.
     * 
     * @return whether subprocess started successfully.
     */
gboolean gsd_fill_operation_run (GsdFillOperation* self, const gchar* directory, GError** error) {
	gboolean result = FALSE;
	const gchar* _tmp0_ = NULL;
	gboolean _tmp2_ = FALSE;
	gboolean _tmp3_ = FALSE;
	GError * _inner_error_ = NULL;
	g_return_val_if_fail (self != NULL, FALSE);
	_tmp0_ = directory;
	if (_tmp0_ != NULL) {
		const gchar* _tmp1_ = NULL;
		_tmp1_ = directory;
		gsd_fill_operation_set_directory (self, _tmp1_);
	}
	_tmp3_ = gsd_secure_delete_operation_run ((GsdSecureDeleteOperation*) G_TYPE_CHECK_INSTANCE_CAST (self, GSD_TYPE_ZEROABLE_OPERATION, GsdZeroableOperation), &_inner_error_);
	_tmp2_ = _tmp3_;
	if (G_UNLIKELY (_inner_error_ != NULL)) {
		if ((_inner_error_->domain == G_SPAWN_ERROR) || (_inner_error_->domain == GSD_ASYNC_OPERATION_ERROR)) {
			g_propagate_error (error, _inner_error_);
			return FALSE;
		} else {
			g_critical ("file %s: line %d: uncaught error: %s (%s, %d)", __FILE__, __LINE__, _inner_error_->message, g_quark_to_string (_inner_error_->domain), _inner_error_->code);
			g_clear_error (&_inner_error_);
			return FALSE;
		}
	}
	result = _tmp2_;
	return result;
}


/**
     * Launches a secure filling synchronously.
     * 
     * @param directory the directory to fill. It is exactly the same as setting
     *    the FillOperation:directory property, just a convenience possibility.
     * 
     * @return whether filling was successful.
     */
gboolean gsd_fill_operation_run_sync (GsdFillOperation* self, const gchar* directory, GError** error) {
	gboolean result = FALSE;
	const gchar* _tmp0_ = NULL;
	gboolean _tmp2_ = FALSE;
	gboolean _tmp3_ = FALSE;
	GError * _inner_error_ = NULL;
	g_return_val_if_fail (self != NULL, FALSE);
	_tmp0_ = directory;
	if (_tmp0_ != NULL) {
		const gchar* _tmp1_ = NULL;
		_tmp1_ = directory;
		gsd_fill_operation_set_directory (self, _tmp1_);
	}
	_tmp3_ = gsd_secure_delete_operation_run_sync ((GsdSecureDeleteOperation*) G_TYPE_CHECK_INSTANCE_CAST (self, GSD_TYPE_ZEROABLE_OPERATION, GsdZeroableOperation), &_inner_error_);
	_tmp2_ = _tmp3_;
	if (G_UNLIKELY (_inner_error_ != NULL)) {
		if ((_inner_error_->domain == G_SPAWN_ERROR) || (_inner_error_->domain == GSD_ASYNC_OPERATION_ERROR)) {
			g_propagate_error (error, _inner_error_);
			return FALSE;
		} else {
			g_critical ("file %s: line %d: uncaught error: %s (%s, %d)", __FILE__, __LINE__, _inner_error_->message, g_quark_to_string (_inner_error_->domain), _inner_error_->code);
			g_clear_error (&_inner_error_);
			return FALSE;
		}
	}
	result = _tmp2_;
	return result;
}


GsdFillOperation* gsd_fill_operation_construct (GType object_type) {
	GsdFillOperation * self = NULL;
	self = (GsdFillOperation*) gsd_zeroable_operation_construct (object_type);
	return self;
}


GsdFillOperation* gsd_fill_operation_new (void) {
	return gsd_fill_operation_construct (GSD_TYPE_FILL_OPERATION);
}


GsdFillOperationWipeMode gsd_fill_operation_get_wipe_mode (GsdFillOperation* self) {
	GsdFillOperationWipeMode result;
	GsdFillOperationWipeMode _tmp0_ = 0;
	g_return_val_if_fail (self != NULL, 0);
	_tmp0_ = self->priv->_wipe_mode;
	result = _tmp0_;
	return result;
}


void gsd_fill_operation_set_wipe_mode (GsdFillOperation* self, GsdFillOperationWipeMode value) {
	GsdFillOperationWipeMode _tmp0_ = 0;
	g_return_if_fail (self != NULL);
	_tmp0_ = value;
	self->priv->_wipe_mode = _tmp0_;
	g_object_notify ((GObject *) self, "wipe-mode");
}


const gchar* gsd_fill_operation_get_directory (GsdFillOperation* self) {
	const gchar* result;
	const gchar* _tmp0_ = NULL;
	g_return_val_if_fail (self != NULL, NULL);
	_tmp0_ = self->priv->_directory;
	result = _tmp0_;
	return result;
}


void gsd_fill_operation_set_directory (GsdFillOperation* self, const gchar* value) {
	const gchar* _tmp0_ = NULL;
	gchar* _tmp1_ = NULL;
	g_return_if_fail (self != NULL);
	_tmp0_ = value;
	_tmp1_ = g_strdup (_tmp0_);
	_g_free0 (self->priv->_directory);
	self->priv->_directory = _tmp1_;
	g_object_notify ((GObject *) self, "directory");
}


static GObject * gsd_fill_operation_constructor (GType type, guint n_construct_properties, GObjectConstructParam * construct_properties) {
	GObject * obj;
	GObjectClass * parent_class;
	GsdFillOperation * self;
	parent_class = G_OBJECT_CLASS (gsd_fill_operation_parent_class);
	obj = parent_class->constructor (type, n_construct_properties, construct_properties);
	self = G_TYPE_CHECK_INSTANCE_CAST (obj, GSD_TYPE_FILL_OPERATION, GsdFillOperation);
	gsd_async_operation_set_path ((GsdAsyncOperation*) self, SFILL_PATH);
	return obj;
}


static void gsd_fill_operation_class_init (GsdFillOperationClass * klass) {
	gsd_fill_operation_parent_class = g_type_class_peek_parent (klass);
	g_type_class_add_private (klass, sizeof (GsdFillOperationPrivate));
	((GsdAsyncOperationClass *) klass)->build_args = gsd_fill_operation_real_build_args;
	((GsdAsyncOperationClass *) klass)->cleanup = gsd_fill_operation_real_cleanup;
	((GsdAsyncOperationClass *) klass)->classify_exit_status = gsd_fill_operation_real_classify_exit_status;
	G_OBJECT_CLASS (klass)->get_property = _vala_gsd_fill_operation_get_property;
	G_OBJECT_CLASS (klass)->set_property = _vala_gsd_fill_operation_set_property;
	G_OBJECT_CLASS (klass)->constructor = gsd_fill_operation_constructor;
	G_OBJECT_CLASS (klass)->finalize = gsd_fill_operation_finalize;
	/**
	     * The wiping mode
	     */
	g_object_class_install_property (G_OBJECT_CLASS (klass), GSD_FILL_OPERATION_WIPE_MODE, g_param_spec_enum ("wipe-mode", "wipe-mode", "wipe-mode", GSD_FILL_OPERATION_TYPE_WIPE_MODE, GSD_FILL_OPERATION_WIPE_MODE_FULL, G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB | G_PARAM_READABLE | G_PARAM_WRITABLE));
	/**
	     * The directory to fill (must of course be on the filesystem to clean)
	     * 
	     * I recommend you not to fill in a useful directory but inside a new and
	     * clean one. This can be useful if the filling is interrupted, voluntarily
	     * or not, as this process may spawn a lot of files that might not be
	     * deleted when interrupted.
	     */
	g_object_class_install_property (G_OBJECT_CLASS (klass), GSD_FILL_OPERATION_DIRECTORY, g_param_spec_string ("directory", "directory", "directory", NULL, G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB | G_PARAM_READABLE | G_PARAM_WRITABLE));
}


static void gsd_fill_operation_instance_init (GsdFillOperation * self) {
	self->priv = GSD_FILL_OPERATION_GET_PRIVATE (self);
	self->priv->_wipe_mode = GSD_FILL_OPERATION_WIPE_MODE_FULL;
	self->priv->_directory = NULL;
}


static void gsd_fill_operation_finalize (GObject* obj) {
	GsdFillOperation * self;
	self = G_TYPE_CHECK_INSTANCE_CAST (obj, GSD_TYPE_FILL_OPERATION, GsdFillOperation);
	_g_free0 (self->priv->_directory);
	G_OBJECT_CLASS (gsd_fill_operation_parent_class)->finalize (obj);
}


/**
   * Wrapper for //sfill//.
   */
GType gsd_fill_operation_get_type (void) {
	static volatile gsize gsd_fill_operation_type_id__volatile = 0;
	if (g_once_init_enter (&gsd_fill_operation_type_id__volatile)) {
		static const GTypeInfo g_define_type_info = { sizeof (GsdFillOperationClass), (GBaseInitFunc) NULL, (GBaseFinalizeFunc) NULL, (GClassInitFunc) gsd_fill_operation_class_init, (GClassFinalizeFunc) NULL, NULL, sizeof (GsdFillOperation), 0, (GInstanceInitFunc) gsd_fill_operation_instance_init, NULL };
		GType gsd_fill_operation_type_id;
		gsd_fill_operation_type_id = g_type_register_static (GSD_TYPE_ZEROABLE_OPERATION, "GsdFillOperation", &g_define_type_info, 0);
		g_once_init_leave (&gsd_fill_operation_type_id__volatile, gsd_fill_operation_type_id);
	}
	return gsd_fill_operation_type_id__volatile;
}


static void _vala_gsd_fill_operation_get_property (GObject * object, guint property_id, GValue * value, GParamSpec * pspec) {
	GsdFillOperation * self;
	self = G_TYPE_CHECK_INSTANCE_CAST (object, GSD_TYPE_FILL_OPERATION, GsdFillOperation);
	switch (property_id) {
		case GSD_FILL_OPERATION_WIPE_MODE:
		g_value_set_enum (value, gsd_fill_operation_get_wipe_mode (self));
		break;
		case GSD_FILL_OPERATION_DIRECTORY:
		g_value_set_string (value, gsd_fill_operation_get_directory (self));
		break;
		default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}


static void _vala_gsd_fill_operation_set_property (GObject * object, guint property_id, const GValue * value, GParamSpec * pspec) {
	GsdFillOperation * self;
	self = G_TYPE_CHECK_INSTANCE_CAST (object, GSD_TYPE_FILL_OPERATION, GsdFillOperation);
	switch (property_id) {
		case GSD_FILL_OPERATION_WIPE_MODE:
		gsd_fill_operation_set_wipe_mode (self, g_value_get_enum (value));
		break;
		case GSD_FILL_OPERATION_DIRECTORY:
		gsd_fill_operation_set_directory (self, g_value_get_string (value));
		break;
		default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}



