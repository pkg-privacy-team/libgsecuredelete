/* 
 * 
 * Copyright (C) 2009-2011 Colomban Wendling <ban@herbesfolles.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

namespace Gsd
{
  /**
   * Wrapper for //sswap//.
   */
  public class SwapOperation : ZeroableOperation
  {
    /**
     * The swap device to wipe.
     */
    public string device {
      get;
      set;
      default = null;
    }
    /**
     * Whether to throw an error if the swap is detected as being currently
     * used.
     * 
     * This may be useful because doing the operation on an in-use swap device
     * may crash the system.
     */
    public bool check_device {
      get;
      set;
      default = true;
    }
    
    /* constructor */
    construct {
      this.path = Config.SSWAP_PATH;
    }
    
    /* checks whether a swap device is currently in use by the system or not.
     * TODO: For now we parse /proc/swaps to know it.. is this portable and/or
     * the correct way to proceed? */
    private bool swap_is_in_use (string swapdev)
    {
      var in_use = false;
      /* cache variable, only not for computing the size more than once */
      var swapdev_len = swapdev.length;
      
      var file = FileStream.open ("/proc/swaps", "r");
      if (file != null) {
        string? line = null;
        
        file.read_line (); /* skip header */
        do {
          line = file.read_line ();
          if (line != null) {
            in_use = line.has_prefix (swapdev) &&
                     /* We don't want to match only the start but the whole
                      * device name, then check the next character for
                      * separator.
                      * No risk of overflow as if we are here @line has the
                      * @swapdev prefix, and the character at
                      * line[swapdev.length] must be at least the string-ending
                      * character (0) */
                     " \t".index_of_char (line[swapdev_len]) != -1;
          }
        } while (line != null && ! in_use);
      }
      
      return in_use;
    }
    
    /* builds the argument vector */
    protected override List<string> build_args ()
      throws AsyncOperationError
      requires (this.device != null)
    {
      List<string> args = null;
      
      /* not really clean to throw AsyncOperationError.SUBCLASS_ERROR, but
       * there's no clean way to do that.. */
      if (this.check_device && this.swap_is_in_use (this.device)) {
        throw new AsyncOperationError.SUBCLASS_ERROR (_("The swap device \"%s\" is in use"),
                                                      this.device);
      }
      
      args = base.build_args ();
      args.append (this.device);
      
      return args;
    }
    
    /* cleans up after subprocess termination */
    protected override void cleanup ()
    {
      this.device = null;
    }
    
    /**
     * Launches a secure wiping of a swap device asynchronously.
     * 
     * @param device the swap device to wipe. It is exactly the same as setting
     *    the SwapOperation:device property, just a convenience shortcut.
     * 
     * @return whether subprocess started successfully.
     */
    public new bool run (string? device = null)
      throws SpawnError, AsyncOperationError
    {
      if (device != null) {
        this.device = device;
      }
      return base.run ();
    }
    
    /**
     * Launches a secure wiping of a swap device synchronously.
     * 
     * @param device the swap device to wipe. It is exactly the same as setting
     *    the SwapOperation:device property, just a convenience shortcut.
     * 
     * @return whether wiping of the swap device succeed.
     */
    public new bool run_sync (string? device = null)
      throws SpawnError, AsyncOperationError
    {
      if (device != null) {
        this.device = device;
      }
      return base.run_sync ();
    }
  }
}
