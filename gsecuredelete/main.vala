/* 
 * 
 * Copyright (C) 2009-2011 Colomban Wendling <ban@herbesfolles.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

using Gsd;

public class Test
{
  private MainLoop main_loop;
  
  public enum Operation {
    DELETE,
    FILL,
    MEM,
    SWAP
  }
  
  [CCode (has_target = false)]
  private delegate SecureDeleteOperation? OperationProvider (string[] args);
  
  private OperationProvider[] operations = {
    prepare_delop,
    prepare_fillop,
    prepare_memop,
    prepare_swapop
  };
  
  public Test ()
  {
    this.main_loop = new MainLoop (null, false);
  }
  
  ~Test ()
  {
    if (main_loop.is_running ()) {
      main_loop.quit ();
    }
  }
  
  void finished_handler (bool    success,
                         string? message)
  {
    if (success) {
      stdout.printf ("Success!\n");
    } else {
      stderr.printf ("Failure: %s\n", message);
    }
    this.main_loop.quit ();
  }
  
  static void progress_handler (double progress)
  {
    stdout.printf ("%.0f%%\n", progress * 100);
  }
  
  private static SecureDeleteOperation? prepare_delop (string[] args)
  {
    var op = new DeleteOperation ();
    
    /*op.mode = SecureDeleteOperation.Mode.INSECURE;*/
    foreach (var arg in args) {
      /* hack to skip the first argument */
      if (arg != args[0])
        op.add_path (arg);
    }
    foreach (string i in op.paths) {
      stdout.printf ("Will delete: %s\n", i);
    }
    
    return op;
  }
  
  private static SecureDeleteOperation? prepare_fillop (string[] args)
  {
    var op = new FillOperation ();
    
    if (args.length != 2) {
      return null;
    }
    op.directory = args[1];
    
    return op;
  }
  
  private static SecureDeleteOperation? prepare_memop (string[] args)
  {
    var op = new MemOperation ();
    op.mode = SecureDeleteOperation.Mode.VERY_INSECURE;
    
    return op;
  }
  
  private static SecureDeleteOperation? prepare_swapop (string[] args)
  {
    var op = new SwapOperation ();
    op.device = "/dev/sda1";
    op.mode = SecureDeleteOperation.Mode.INSECURE;
    
    return op;
  }
  
  public bool run (string[]   args,
                   Operation  operation)
  {
    var op = this.operations[operation] (args);
    
    if (op == null) {
      return false;
    }
    op.finished.connect (finished_handler);
    op.progress.connect (progress_handler);
    try {
      op.run ();
      this.main_loop.run ();
    } catch (Error e) {
      stderr.printf ("failed to start deletion: %s\n", e.message);
    }
    
    return true;
  }
}

public static int main (string[] args)
{
  var test = new Test ();
  
  return test.run (args, Test.Operation.DELETE) ? 0 : 1;
}
