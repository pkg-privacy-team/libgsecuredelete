/* 
 * 
 * Copyright (C) 2009-2011 Colomban Wendling <ban@herbesfolles.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

using GLib;

namespace Gsd
{
  
  /**
   * A SecureDeleteOperation subclass to implement zeroable operations
   * 
   * A base class for all SecureDelete operations that support wiping with zeros
   * as the last pass.
   */
  public abstract class ZeroableOperation : SecureDeleteOperation
  {
    /**
     * Whether to wipe with zeros at the last pass instead of random data.
     */
    public bool zeroise {
      get;
      set;
      default = false;
    }
    
    /* returns the argument to pass to SecureDelete tools depending whether the
     * last wiping pass should be done with zeros. %null wan be returned,
     * meaning no argument should be added */
    private unowned string? get_argument_for_zeroise (bool zeroise)
    {
      if (zeroise) {
        return "-z";
      }
      return null;
    }
    
    /* adds the argument needed by the currently set zeorise setting to an
     * argument list */
    private void add_zeroise_argument (ref List<string> args)
    {
      unowned string? arg = this.get_argument_for_zeroise (this.zeroise);
      if (arg != null) {
        args.append (arg);
      }
    }
    
    protected override List<string> build_args ()
      throws AsyncOperationError
    {
      var args = base.build_args ();
      
      this.add_zeroise_argument (ref args);
      
      return args;
    }
  }
}
