/* 
 * 
 * Copyright (C) 2009-2011 Colomban Wendling <ban@herbesfolles.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

namespace Gsd
{
  /**
   * Wrapper for //sfill//.
   */
  public class FillOperation : ZeroableOperation
  {
    /**
     * What to wipe
     */
    public enum WipeMode {
      /**
       * wipe both free inode and data free space.
       */
      FULL,
      /**
       * wipe only free inode space (not free data space)
       */
      INODES,
      /**
       * wipe only free data space (not free inode space)
       */
      DATA
    }
    
    /**
     * The wiping mode
     */
    public WipeMode wipe_mode {
      get;
      set;
      default = WipeMode.FULL;
    }
    /**
     * The directory to fill (must of course be on the filesystem to clean)
     * 
     * I recommend you not to fill in a useful directory but inside a new and
     * clean one. This can be useful if the filling is interrupted, voluntarily
     * or not, as this process may spawn a lot of files that might not be
     * deleted when interrupted.
     */
    public string directory {
      get;
      set;
      default = null;
    }
    
    /* constructor */
    construct {
      this.path = Config.SFILL_PATH;
    }
    
    /* returns the argument needed for a wipe mode */
    private unowned string? get_argument_for_wipe_mode (WipeMode wipe_mode)
    {
      switch (wipe_mode) {
        case WipeMode.INODES:
          return "-i";
        case WipeMode.DATA:
          return "-I";
      }
      return null;
    }
    
    /* adds the argument needed by the current wipe mode to an argument list */
    private void add_wipe_mode_argument (ref List<string> args)
    {
      unowned string? arg_mode = this.get_argument_for_wipe_mode (this.wipe_mode);
      if (arg_mode != null) {
        args.append (arg_mode);
      }
    }
    
    /* builds the argument vector */
    protected override List<string> build_args ()
      throws AsyncOperationError
      requires (this.directory != null)
    {
      List<string> args = null;
      
      args = base.build_args ();
      this.add_wipe_mode_argument (ref args);
      args.append (this.directory);
      
      return args;
    }
    
    /* cleans up after subprocess termination */
    protected override void cleanup ()
    {
      this.directory = null;
    }
    
    protected override AsyncOperation.ExitStatus classify_exit_status (int exit_status)
    {
      switch (exit_status) {
        case 0:   return ExitStatus.SUCCESS;
        /* gsd-sfill-helper special case */
        case 42:  return ExitStatus.WARNING;
        default:  return ExitStatus.ERROR;
      }
    }
    
    /**
     * Launches a secure filling asynchronously.
     * 
     * @param directory the directory to fill. It is exactly the same as setting
     *    the FillOperation:directory property, just a convenience possibility.
     * 
     * @return whether subprocess started successfully.
     */
    public new bool run (string? directory = null)
      throws SpawnError, AsyncOperationError
    {
      if (directory != null) {
        this.directory = directory;
      }
      return base.run ();
    }
    
    /**
     * Launches a secure filling synchronously.
     * 
     * @param directory the directory to fill. It is exactly the same as setting
     *    the FillOperation:directory property, just a convenience possibility.
     * 
     * @return whether filling was successful.
     */
    public new bool run_sync (string? directory = null)
      throws SpawnError, AsyncOperationError
    {
      if (directory != null) {
        this.directory = directory;
      }
      return base.run_sync ();
    }
  }
}
