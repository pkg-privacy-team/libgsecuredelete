
[CCode (cprefix = "", lower_case_cprefix = "", cheader_filename = "gsd-config.h")]
namespace Gsd.Config
{
  public const string PACKAGE;
  public const string VERSION;
  /* i18n */
  public const string GETTEXT_PACKAGE;
  public const string LOCALEDIR;
  /* program paths */
  public const string SRM_PATH;
  public const string SFILL_PATH;
  public const string SMEM_PATH;
  public const string SSWAP_PATH;
}
