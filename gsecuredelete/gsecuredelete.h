/* gsecuredelete.h generated by valac 0.34.2, the Vala compiler, do not modify */


#ifndef __GSECUREDELETE_H__
#define __GSECUREDELETE_H__

#include <glib.h>
#include <glib-object.h>
#include <stdlib.h>
#include <string.h>

G_BEGIN_DECLS


#define GSD_TYPE_ASYNC_OPERATION (gsd_async_operation_get_type ())
#define GSD_ASYNC_OPERATION(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), GSD_TYPE_ASYNC_OPERATION, GsdAsyncOperation))
#define GSD_ASYNC_OPERATION_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), GSD_TYPE_ASYNC_OPERATION, GsdAsyncOperationClass))
#define GSD_IS_ASYNC_OPERATION(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GSD_TYPE_ASYNC_OPERATION))
#define GSD_IS_ASYNC_OPERATION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GSD_TYPE_ASYNC_OPERATION))
#define GSD_ASYNC_OPERATION_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), GSD_TYPE_ASYNC_OPERATION, GsdAsyncOperationClass))

typedef struct _GsdAsyncOperation GsdAsyncOperation;
typedef struct _GsdAsyncOperationClass GsdAsyncOperationClass;
typedef struct _GsdAsyncOperationPrivate GsdAsyncOperationPrivate;

#define GSD_ASYNC_OPERATION_TYPE_EXIT_STATUS (gsd_async_operation_exit_status_get_type ())

#define GSD_TYPE_SECURE_DELETE_OPERATION (gsd_secure_delete_operation_get_type ())
#define GSD_SECURE_DELETE_OPERATION(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), GSD_TYPE_SECURE_DELETE_OPERATION, GsdSecureDeleteOperation))
#define GSD_SECURE_DELETE_OPERATION_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), GSD_TYPE_SECURE_DELETE_OPERATION, GsdSecureDeleteOperationClass))
#define GSD_IS_SECURE_DELETE_OPERATION(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GSD_TYPE_SECURE_DELETE_OPERATION))
#define GSD_IS_SECURE_DELETE_OPERATION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GSD_TYPE_SECURE_DELETE_OPERATION))
#define GSD_SECURE_DELETE_OPERATION_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), GSD_TYPE_SECURE_DELETE_OPERATION, GsdSecureDeleteOperationClass))

typedef struct _GsdSecureDeleteOperation GsdSecureDeleteOperation;
typedef struct _GsdSecureDeleteOperationClass GsdSecureDeleteOperationClass;
typedef struct _GsdSecureDeleteOperationPrivate GsdSecureDeleteOperationPrivate;

#define GSD_TYPE_ZEROABLE_OPERATION (gsd_zeroable_operation_get_type ())
#define GSD_ZEROABLE_OPERATION(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), GSD_TYPE_ZEROABLE_OPERATION, GsdZeroableOperation))
#define GSD_ZEROABLE_OPERATION_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), GSD_TYPE_ZEROABLE_OPERATION, GsdZeroableOperationClass))
#define GSD_IS_ZEROABLE_OPERATION(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GSD_TYPE_ZEROABLE_OPERATION))
#define GSD_IS_ZEROABLE_OPERATION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GSD_TYPE_ZEROABLE_OPERATION))
#define GSD_ZEROABLE_OPERATION_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), GSD_TYPE_ZEROABLE_OPERATION, GsdZeroableOperationClass))

typedef struct _GsdZeroableOperation GsdZeroableOperation;
typedef struct _GsdZeroableOperationClass GsdZeroableOperationClass;
typedef struct _GsdZeroableOperationPrivate GsdZeroableOperationPrivate;

#define GSD_TYPE_DELETE_OPERATION (gsd_delete_operation_get_type ())
#define GSD_DELETE_OPERATION(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), GSD_TYPE_DELETE_OPERATION, GsdDeleteOperation))
#define GSD_DELETE_OPERATION_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), GSD_TYPE_DELETE_OPERATION, GsdDeleteOperationClass))
#define GSD_IS_DELETE_OPERATION(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GSD_TYPE_DELETE_OPERATION))
#define GSD_IS_DELETE_OPERATION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GSD_TYPE_DELETE_OPERATION))
#define GSD_DELETE_OPERATION_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), GSD_TYPE_DELETE_OPERATION, GsdDeleteOperationClass))

typedef struct _GsdDeleteOperation GsdDeleteOperation;
typedef struct _GsdDeleteOperationClass GsdDeleteOperationClass;
typedef struct _GsdDeleteOperationPrivate GsdDeleteOperationPrivate;

#define GSD_TYPE_FILL_OPERATION (gsd_fill_operation_get_type ())
#define GSD_FILL_OPERATION(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), GSD_TYPE_FILL_OPERATION, GsdFillOperation))
#define GSD_FILL_OPERATION_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), GSD_TYPE_FILL_OPERATION, GsdFillOperationClass))
#define GSD_IS_FILL_OPERATION(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GSD_TYPE_FILL_OPERATION))
#define GSD_IS_FILL_OPERATION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GSD_TYPE_FILL_OPERATION))
#define GSD_FILL_OPERATION_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), GSD_TYPE_FILL_OPERATION, GsdFillOperationClass))

typedef struct _GsdFillOperation GsdFillOperation;
typedef struct _GsdFillOperationClass GsdFillOperationClass;
typedef struct _GsdFillOperationPrivate GsdFillOperationPrivate;

#define GSD_FILL_OPERATION_TYPE_WIPE_MODE (gsd_fill_operation_wipe_mode_get_type ())

#define GSD_TYPE_MEM_OPERATION (gsd_mem_operation_get_type ())
#define GSD_MEM_OPERATION(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), GSD_TYPE_MEM_OPERATION, GsdMemOperation))
#define GSD_MEM_OPERATION_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), GSD_TYPE_MEM_OPERATION, GsdMemOperationClass))
#define GSD_IS_MEM_OPERATION(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GSD_TYPE_MEM_OPERATION))
#define GSD_IS_MEM_OPERATION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GSD_TYPE_MEM_OPERATION))
#define GSD_MEM_OPERATION_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), GSD_TYPE_MEM_OPERATION, GsdMemOperationClass))

typedef struct _GsdMemOperation GsdMemOperation;
typedef struct _GsdMemOperationClass GsdMemOperationClass;
typedef struct _GsdMemOperationPrivate GsdMemOperationPrivate;

#define GSD_SECURE_DELETE_OPERATION_TYPE_MODE (gsd_secure_delete_operation_mode_get_type ())

#define GSD_TYPE_SWAP_OPERATION (gsd_swap_operation_get_type ())
#define GSD_SWAP_OPERATION(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), GSD_TYPE_SWAP_OPERATION, GsdSwapOperation))
#define GSD_SWAP_OPERATION_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), GSD_TYPE_SWAP_OPERATION, GsdSwapOperationClass))
#define GSD_IS_SWAP_OPERATION(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GSD_TYPE_SWAP_OPERATION))
#define GSD_IS_SWAP_OPERATION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GSD_TYPE_SWAP_OPERATION))
#define GSD_SWAP_OPERATION_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), GSD_TYPE_SWAP_OPERATION, GsdSwapOperationClass))

typedef struct _GsdSwapOperation GsdSwapOperation;
typedef struct _GsdSwapOperationClass GsdSwapOperationClass;
typedef struct _GsdSwapOperationPrivate GsdSwapOperationPrivate;

typedef enum  {
	GSD_ASYNC_OPERATION_ERROR_CHILD_FAILED,
	GSD_ASYNC_OPERATION_ERROR_SUBCLASS_ERROR
} GsdAsyncOperationError;
#define GSD_ASYNC_OPERATION_ERROR gsd_async_operation_error_quark ()
typedef enum  {
	GSD_ASYNC_OPERATION_EXIT_STATUS_SUCCESS,
	GSD_ASYNC_OPERATION_EXIT_STATUS_WARNING,
	GSD_ASYNC_OPERATION_EXIT_STATUS_ERROR
} GsdAsyncOperationExitStatus;

struct _GsdAsyncOperation {
	GObject parent_instance;
	GsdAsyncOperationPrivate * priv;
	guint n_passes;
	guint passes;
	GPid pid;
	gint fd_in;
	gint fd_out;
	gint fd_err;
};

struct _GsdAsyncOperationClass {
	GObjectClass parent_class;
	GList* (*build_args) (GsdAsyncOperation* self, GError** error);
	gchar** (*build_env) (GsdAsyncOperation* self, int* result_length1);
	void (*cleanup) (GsdAsyncOperation* self);
	guint (*get_max_progress) (GsdAsyncOperation* self);
	guint (*get_progress) (GsdAsyncOperation* self);
	gchar* (*get_subprocess_error_msg) (GsdAsyncOperation* self);
	GsdAsyncOperationExitStatus (*classify_exit_status) (GsdAsyncOperation* self, gint exit_status);
};

struct _GsdSecureDeleteOperation {
	GsdAsyncOperation parent_instance;
	GsdSecureDeleteOperationPrivate * priv;
};

struct _GsdSecureDeleteOperationClass {
	GsdAsyncOperationClass parent_class;
};

struct _GsdZeroableOperation {
	GsdSecureDeleteOperation parent_instance;
	GsdZeroableOperationPrivate * priv;
};

struct _GsdZeroableOperationClass {
	GsdSecureDeleteOperationClass parent_class;
};

struct _GsdDeleteOperation {
	GsdZeroableOperation parent_instance;
	GsdDeleteOperationPrivate * priv;
};

struct _GsdDeleteOperationClass {
	GsdZeroableOperationClass parent_class;
};

struct _GsdFillOperation {
	GsdZeroableOperation parent_instance;
	GsdFillOperationPrivate * priv;
};

struct _GsdFillOperationClass {
	GsdZeroableOperationClass parent_class;
};

typedef enum  {
	GSD_FILL_OPERATION_WIPE_MODE_FULL,
	GSD_FILL_OPERATION_WIPE_MODE_INODES,
	GSD_FILL_OPERATION_WIPE_MODE_DATA
} GsdFillOperationWipeMode;

struct _GsdMemOperation {
	GsdSecureDeleteOperation parent_instance;
	GsdMemOperationPrivate * priv;
};

struct _GsdMemOperationClass {
	GsdSecureDeleteOperationClass parent_class;
};

typedef enum  {
	GSD_SECURE_DELETE_OPERATION_MODE_NORMAL,
	GSD_SECURE_DELETE_OPERATION_MODE_INSECURE,
	GSD_SECURE_DELETE_OPERATION_MODE_VERY_INSECURE
} GsdSecureDeleteOperationMode;

struct _GsdSwapOperation {
	GsdZeroableOperation parent_instance;
	GsdSwapOperationPrivate * priv;
};

struct _GsdSwapOperationClass {
	GsdZeroableOperationClass parent_class;
};


GQuark gsd_async_operation_error_quark (void);
GType gsd_async_operation_get_type (void) G_GNUC_CONST;
GType gsd_async_operation_exit_status_get_type (void) G_GNUC_CONST;
GList* gsd_async_operation_build_args (GsdAsyncOperation* self, GError** error);
gchar** gsd_async_operation_build_env (GsdAsyncOperation* self, int* result_length1);
void gsd_async_operation_cleanup (GsdAsyncOperation* self);
guint gsd_async_operation_get_max_progress (GsdAsyncOperation* self);
guint gsd_async_operation_get_progress (GsdAsyncOperation* self);
gchar* gsd_async_operation_get_subprocess_error_msg (GsdAsyncOperation* self);
GsdAsyncOperationExitStatus gsd_async_operation_classify_exit_status (GsdAsyncOperation* self, gint exit_status);
gboolean gsd_async_operation_cancel (GsdAsyncOperation* self);
gboolean gsd_async_operation_pause (GsdAsyncOperation* self);
gboolean gsd_async_operation_resume (GsdAsyncOperation* self);
gboolean gsd_async_operation_run (GsdAsyncOperation* self, const gchar* working_directory, GSpawnFlags spawn_flags, GError** error);
gboolean gsd_async_operation_run_sync (GsdAsyncOperation* self, const gchar* working_directory, GSpawnFlags spawn_flags, gchar** standard_output, GError** error);
GsdAsyncOperation* gsd_async_operation_construct (GType object_type);
const gchar* gsd_async_operation_get_path (GsdAsyncOperation* self);
void gsd_async_operation_set_path (GsdAsyncOperation* self, const gchar* value);
gboolean gsd_async_operation_get_busy (GsdAsyncOperation* self);
GType gsd_secure_delete_operation_get_type (void) G_GNUC_CONST;
GType gsd_zeroable_operation_get_type (void) G_GNUC_CONST;
GType gsd_delete_operation_get_type (void) G_GNUC_CONST;
void gsd_delete_operation_add_path (GsdDeleteOperation* self, const gchar* path);
void gsd_delete_operation_remove_path (GsdDeleteOperation* self, const gchar* path);
GsdDeleteOperation* gsd_delete_operation_new (void);
GsdDeleteOperation* gsd_delete_operation_construct (GType object_type);
GList* gsd_delete_operation_get_paths (GsdDeleteOperation* self);
GType gsd_fill_operation_get_type (void) G_GNUC_CONST;
GType gsd_fill_operation_wipe_mode_get_type (void) G_GNUC_CONST;
gboolean gsd_fill_operation_run (GsdFillOperation* self, const gchar* directory, GError** error);
gboolean gsd_fill_operation_run_sync (GsdFillOperation* self, const gchar* directory, GError** error);
GsdFillOperation* gsd_fill_operation_new (void);
GsdFillOperation* gsd_fill_operation_construct (GType object_type);
GsdFillOperationWipeMode gsd_fill_operation_get_wipe_mode (GsdFillOperation* self);
void gsd_fill_operation_set_wipe_mode (GsdFillOperation* self, GsdFillOperationWipeMode value);
const gchar* gsd_fill_operation_get_directory (GsdFillOperation* self);
void gsd_fill_operation_set_directory (GsdFillOperation* self, const gchar* value);
void gsd_intl_init (void);
GType gsd_mem_operation_get_type (void) G_GNUC_CONST;
GsdMemOperation* gsd_mem_operation_new (void);
GsdMemOperation* gsd_mem_operation_construct (GType object_type);
GType gsd_secure_delete_operation_mode_get_type (void) G_GNUC_CONST;
gboolean gsd_secure_delete_operation_run (GsdSecureDeleteOperation* self, GError** error);
gboolean gsd_secure_delete_operation_run_sync (GsdSecureDeleteOperation* self, GError** error);
GsdSecureDeleteOperation* gsd_secure_delete_operation_construct (GType object_type);
gboolean gsd_secure_delete_operation_get_fast (GsdSecureDeleteOperation* self);
void gsd_secure_delete_operation_set_fast (GsdSecureDeleteOperation* self, gboolean value);
GsdSecureDeleteOperationMode gsd_secure_delete_operation_get_mode (GsdSecureDeleteOperation* self);
void gsd_secure_delete_operation_set_mode (GsdSecureDeleteOperation* self, GsdSecureDeleteOperationMode value);
GType gsd_swap_operation_get_type (void) G_GNUC_CONST;
gboolean gsd_swap_operation_run (GsdSwapOperation* self, const gchar* device, GError** error);
gboolean gsd_swap_operation_run_sync (GsdSwapOperation* self, const gchar* device, GError** error);
GsdSwapOperation* gsd_swap_operation_new (void);
GsdSwapOperation* gsd_swap_operation_construct (GType object_type);
const gchar* gsd_swap_operation_get_device (GsdSwapOperation* self);
void gsd_swap_operation_set_device (GsdSwapOperation* self, const gchar* value);
gboolean gsd_swap_operation_get_check_device (GsdSwapOperation* self);
void gsd_swap_operation_set_check_device (GsdSwapOperation* self, gboolean value);
GsdZeroableOperation* gsd_zeroable_operation_construct (GType object_type);
gboolean gsd_zeroable_operation_get_zeroise (GsdZeroableOperation* self);
void gsd_zeroable_operation_set_zeroise (GsdZeroableOperation* self, gboolean value);


G_END_DECLS

#endif
