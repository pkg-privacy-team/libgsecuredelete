/* 
 * 
 * Copyright (C) 2009-2011 Colomban Wendling <ban@herbesfolles.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

using GLib;

namespace Gsd
{
  /**
   * Wrapper for //srm//.
   */
  public class DeleteOperation : ZeroableOperation
  {
    private List<string>  _paths; /* the list of paths to remove */
    
    /* properties */
    /**
     * List of all files to wipe.
     */
    public List<string> paths {
      get {
        return this._paths;
      }
    }
    
    /* constructor */
    construct {
      this.path = Config.SRM_PATH;
    }
    
    /**
     * Adds a path to the list of paths to remove.
     * 
     * @param path a path to securely remove.
     */
    public void add_path (string path)
      requires (! this.busy)
    {
      this._paths.append (path);
    }
    
    /**
     * Removes a path from the list of paths to wipe.
     * 
     * Note that this is NOT a filter for files not to wipe! It only remove a
     * path previously added with add_path() from the list of paths to wipe, it
     * does not prevent wiping of a path if the directory containing it is in
     * the list of paths to wipe.
     * 
     * @param path a path already added you want to remove from the list.
     */
    public void remove_path (string path)
      requires (! this.busy)
    {
      /* FIXME: support for canonical names?
       * (e.g. /aa/../bb is the same as /bb) */
      weak List<string> link;
      
      link = this._paths.find_custom (path,
                                      (a, b) => {
                                        return (string)a == (string)b ? 0 : 1;
                                      });
      if (link != null) {
        this._paths.delete_link (link);
      }
    }
    
    /* We override the base one to insert a hack:
     * We add a "\n" after a special message because srm writes it on stdout
     * rather than stderr, then we miss it. this is not easily fixable in the
     * srm side without getting a not-so-nice output with mixed stdout and
     * stderr.
     * Pretty funny hack! */
    protected override string? get_subprocess_error_msg ()
    {
      /* this string is very unlikely to appear somewhere we don't want to
       * replace it, and even if it appears it is not a real problem to replace
       * it */
      unowned string pattern = "rename/unlink only!";
      
      return base.get_subprocess_error_msg ().replace (pattern, pattern + "\n");
    }
    
    /* Counts regular files, starting from @path.
     * Used to get the actual count of files we shall delete (and then the
     * actual final progress) because srm reports the progress for each and
     * every file it deletes, even if they were inside a directory we given.
     * Note that there is no progress for directories at all, then skip them;
     * just keep the files they holds.
     * Finally, note that srm deletes only regular files (fails on hard links,
     * renames soft links only, etc.).
     * 
     * Even if somewhat overkill, it should not have (a too big) impact on speed
     * since srm shall walk the directories anyway then we might have cached
     * them already, and the srm work is slow anyway.
     * Then keep goin', we're happy with that. */
    private uint count_files (string path)
    {
      uint n_files = 0;
      
      if (FileUtils.test (path, FileTest.IS_DIR)) {
        try {
          var             dir = Dir.open (path);
          unowned string  name;
          
          while ((name = dir.read_name ()) != null) {
            n_files += count_files (Path.build_filename (path, name, null));
          }
        } catch (FileError e) {
          /* silently fail, doesn't matter. we just guess anyway */
          /*warning ("** %s", e.message);*/
        }
      } else if (FileUtils.test (path, FileTest.IS_SYMLINK)) {
        /* skip this one silently too */
      } else {
        n_files++;
      }
      
      return n_files;
    }
    
    /* builds the command's argument vector */
    protected override List<string> build_args ()
      throws AsyncOperationError
      /* FIXME: use an optimization here like (this._paths != null) not to walk
       * the entire list? Faster but a bit ugly... */
      requires (this._paths.length () > 0)
    {
      List<string> args = null;
      
      args = base.build_args ();
      args.append ("-r");
      foreach (unowned string path in this._paths) {
        args.append (path);
      }
      
      return args;
    }
    
    protected override void cleanup ()
    {
      /* empty the path list not to try to wipe them again at next call */
      this._paths = null;
    }
    
    protected override uint get_max_progress ()
    {
      uint n_files = 0;
      
      foreach (unowned string path in this._paths) {
        n_files += this.count_files (path);
      }
      
      return base.get_max_progress () * n_files;
    }
  }
}
