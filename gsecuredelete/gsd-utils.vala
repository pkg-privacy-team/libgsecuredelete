/* 
 * 
 * Copyright (C) 2009-2011 Colomban Wendling <ban@herbesfolles.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

namespace Gsd
{
  /**
   * Error domain for the FD namespace functions. It is mainly a wrapper around
   * some errnos to have exceptions for them.
   */
  internal errordomain FDError {
    /**
     * A call to Posix.kill() failed.
     */
    KILL_ERROR,
    /**
     * A call to Posix.read() failed.
     */
    READ_ERROR,
    /**
     * A call to Posix.select() failed.
     */
    SELECT_ERROR,
    /**
     * A call to Posix.waitpid() failed.
     */
    WAITPID_ERROR,
  }
  
  namespace FD
  {
    /**
     * Gets whether a file descriptor is ready to be read or not.
     * 
     * Being ready means that reading data from it would not be blocking, as
     * there is data to be read.
     * 
     * @param fd a file descriptor
     * 
     * @return %TRUE if the file is read, %FALSE otherwise.
     */
    internal bool read_ready (int fd)
      throws FDError
    {
      bool ready = false;
      
      if (fd > 0) {
        Posix.fd_set  rfds = {};
        /* FIXME: is waiting 0ms a portable thing not for blocking? */
        Posix.timeval tv = {0, 0};
        
        Posix.FD_ZERO (out rfds);
        Posix.FD_SET (fd, ref rfds);
        var rv = Posix.select (fd + 1, &rfds, null, null, tv);
        if (rv == -1) {
          throw new FDError.SELECT_ERROR ("select(): %s", strerror (errno));
        } else if (rv > 0) {
          ready = true;
        }
      }
      
      return ready;
    }
    
    /**
     * Reads content of a file as a string.
     * 
     * This function may only work with text data.
     * 
     * @param fd a file descriptor
     * @param n_bytes number of bytes to read from @fd, or -1 to read all @fd
     * 
     * @return a newly allocated string containing data in @fd.
     */
    internal static string? read_string (int     fd,
                                         ssize_t n_bytes = -1)
      throws FDError
    {
      char* buf = null;
      
      if (n_bytes > 0) {
        ssize_t n_read;
        
        buf = new char[n_bytes + 1];
        n_read = Posix.read (fd, buf, n_bytes);
        if (n_read < 0) {
          int errn = errno;
          
          delete buf;
          buf = null;
          throw new FDError.READ_ERROR ("read(): %s", strerror (errn));
        } else {
          buf[n_read] = 0;
        }
      } else {
        size_t  packet_size = 64;
        size_t  n_read = 0;
        ssize_t read_rv = 0;
        
        n_bytes = 0;
        do {
          /* no realloc in Vala, but should be safe */
          buf = realloc (buf, n_bytes + packet_size + 1);
          
          read_rv = Posix.read (fd, &buf[n_bytes], packet_size);
          if (read_rv >= 0) {
            n_read += read_rv;
          }
          
          n_bytes += (ssize_t)packet_size;
        } while (n_bytes > 0 /* don't underflow */ &&
                 read_rv >= 0 &&
                 read_rv == packet_size);
        if (read_rv < 0) {
          int errn = errno;
          
          delete buf;
          buf = null;
          throw new FDError.READ_ERROR ("read(): %s", strerror (errn));
        } else {
          buf[n_read] = 0;
        }
      }
      
      /* pass ownership to the caller */
      return (string?) (owned) buf;
    }
    
    /**
     * Counts the number of occurrence of a given byte in a file descriptor.
     * 
     * This function is non-blocking and returns 0 if there is no data in the
     * buffer rather than blocking waiting for data.
     * 
     * @param fd a valid file descriptor
     * @param byte the byte to cont in @fd
     * @param bufsize the size of the buffer used to cache the file's content.
     *    Tweaking it may improve the performances with different amount of data
     *    in @fd: the better value is the closer to the total number of bytes
     *    that will be read (the size of the file's content) or a multiple of it
     *    if it is a too big value to be reasonably allocated at once.
     * 
     * @return the number of occurrences of @byte in @fd.
     */
    internal uint count_ready_bytes (int    fd,
                                     int    byte,
                                     size_t bufsize = 16)
      throws FDError
    {
      uint n_occur = 0;
      
      try {
        if (FD.read_ready (fd)) {
          char[]  buf = new char[bufsize];
          ssize_t read_rv = 0;
          
          do {
            read_rv = Posix.read (fd, buf, bufsize);
            if (read_rv < 0) {
              throw new FDError.READ_ERROR ("read(): %s", strerror (errno));
            } else {
              for (uint i = 0; i < read_rv; i++) {
                if (buf[i] == byte)
                  n_occur ++;
              }
            }
          } while (read_rv == bufsize);
        }
      } catch (FDError e) {
        throw e;
      }
      
      return n_occur;
    }
  }
}
