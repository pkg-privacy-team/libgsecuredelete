libgsecuredelete (0.3-3.2) unstable; urgency=medium

  * Non-maintainer upload
  * Drop explicit Depends: libglib2.0-0 (Closes: #1067936)
  * Drop non-existing Homepage

 -- Bastian Germann <bage@debian.org>  Sat, 06 Apr 2024 16:55:41 +0000

libgsecuredelete (0.3-3.1) unstable; urgency=medium

  * Non-maintainer upload
  * Recommend pkexec instead of policykit-1 (Closes: #1025576)

 -- Bastian Germann <bage@debian.org>  Thu, 07 Sep 2023 23:37:52 +0000

libgsecuredelete (0.3-3) unstable; urgency=medium

  * Patch gsecuredelete/Makefile.am to generate a proper valac call - Thanks
    Rico Tzschichholz for the patch (Closes: #976579)
  * Install vapi file in proper directory
  * Bump dh compat to 13 and remove debian/compat
  * Replace obsolete dh_install option fail-missing by dh_missing call
  * Add Build-depends-package meta-information to symbol file
  * Switch to Debian email address in Uploaders
  * Add upstream/metadata file
  * Remove obsolete lintian-override (no-upstream-changelog)
  * Force removal of *_vala.stamp files to ensure sources are actually
    generated
  * Use https url in watch file
  * Set Rules-Requires-Root to no
  * Update Copyright information for debian/*
  * Fix libexec install path
  * Install gsd-sfill-helper in LFS compliant directory
  * Declare compliance with Debian Policy 4.5.1
  * Bump watch file to version 4
  * Mark 0005-gsecuredelete_makefile.am-fix-valac-call-gen.patch as forwarded
  * Use https URL in d/copyright
  * Update upstream signing key

 -- Clément Hermann <nodens@debian.org>  Tue, 12 Jan 2021 15:17:08 +0100

libgsecuredelete (0.3-2) unstable; urgency=medium

  * Team upload

  [ Clément Hermann ]
  * d/control:
    - remove intrigeri from uploaders, add myself
    - Fix Vcs-* to point to Salsa

 -- intrigeri <intrigeri@debian.org>  Fri, 21 Aug 2020 11:52:45 +0000

libgsecuredelete (0.3-1) unstable; urgency=medium

  * Import new upstream release.
  * Bump copyright years on debian/*.
  * Sort Build-Depends.
  * Add build-dependency on valac.
  * 0001-configure.ac-set-AM_GNU_GETTEXT_VERSION-0.19.patch:
    new patch, to allow building.
  * Install translations (MO) files.
  * debian/copyright: gsd-sfill-helper.in → gsd-sfill-helper.sh.in
    (renamed upstream).
  * Add new symbols to libgsecuredelete0.symbols.
  * libgsecuredelete0.symbols: bump all versions to 0.3; the
    _GsdAsyncOperationClass struct has changed, and it's used
    basically everywhere.

 -- intrigeri <intrigeri@debian.org>  Wed, 07 Dec 2016 16:04:01 +0000

libgsecuredelete (0.2.1-4) unstable; urgency=medium

  [ Ulrike Uhlig ]
  * Hand over package to pkg-privacy team.

 -- intrigeri <intrigeri@debian.org>  Fri, 08 Jul 2016 15:26:35 +0000

libgsecuredelete (0.2.1-3) unstable; urgency=medium

  * Bump debhelper compatibility level to 9.
  * debian/*.install: adjust to multiarch paths.
  * Drop manual disabling of automake silent rules: debhelper now does this
    automatically.
  * Declare compliance with Standards-Version 3.9.8.
  * Vcs-* and Homepage control fields, debian/copyright:
    use https:// canonical URLs.

 -- intrigeri <intrigeri@debian.org>  Sat, 25 Jun 2016 14:14:47 +0000

libgsecuredelete (0.2.1-2) unstable; urgency=medium

  * Add Recommends from libgsecuredelete0 on policykit-1: pkexec is needed
    for the fill operation to obtain root privileges on non-FAT FS.
  * Move upstream signing key to the place uscan now looks for it.
  * Ship armored upstream OpenPGP key instead of binary one,
    accordingly drop debian/source/include-binaries.
  * Disable automake silent rules.

 -- intrigeri <intrigeri@debian.org>  Fri, 29 Aug 2014 11:22:31 -0700

libgsecuredelete (0.2.1-1) unstable; urgency=medium

  * Imported Upstream version 0.2.1
  * Invoke dh_install with --fail-missing, and explicitly exclude .la files.
  * Use dh-autoreconf to get new libtool macros for ppc64el.
    Thanks to Logan Rosen <logan@ubuntu.com> for the patch.
  * Drop override for buggy Lintian warning that was fixed since then.
  * Reformat debian/control with cme.
  * Declare compliance with standards 3.9.5.
  * Add OpenPGP signature checking support to debian/watch.
  * Use canonical URL for Vcs-Git.
  * Make libgsecuredelete-dev depend on a real package first,
    that is libc6-dev | libc-dev.

 -- intrigeri <intrigeri@debian.org>  Fri, 24 Jan 2014 15:38:08 +0100

libgsecuredelete (0.2-1) unstable; urgency=high

  * New upstream release. Major changes are:
    - Workaround sfill limitations regarding secure deletion
      of free space larger than the maximum file size supported
      by the filesystem. (Closes: #678208)
    - Fix delete operation progress.
  * Update specification URL in debian/copyright.
  * Bump Standards-Version to 3.9.3, no change required.
  * Setting priority to high to get the fix of secure deletion of
    free space (that is particularly problematic on FAT, due to its
    4GB file size limit) in sooner.
  * Install gsd-sfill-helper in /usr/lib/libgsecuredelete0/libexec/.
  * Add copyright information for gsd-sfill-helper.

 -- intrigeri <intrigeri@debian.org>  Tue, 19 Jun 2012 23:46:07 +0200

libgsecuredelete (0.1-3) unstable; urgency=low

  * Use dpkg-buildflags instead of hardening-wrapper.
  * Set my email address to my shiny new @debian.org one.

 -- intrigeri <intrigeri@debian.org>  Sun, 22 Jan 2012 15:07:10 +0100

libgsecuredelete (0.1-2) unstable; urgency=low

  * Initial release. (Closes: #636041)
  * Update Vcs-* control fields: packaging repo was moved to collab-maint.
  * Fix my name.
  * Add explicit dependency from the -dev package on libc-dev.
  * Add Lintian overrides for false positives.

 -- intrigeri <intrigeri+debian@boum.org>  Sun, 30 Oct 2011 14:20:23 +0100
